<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function stopConfirm(){
	myRet = window.confirm("このユーザーのアカウントを停止します。よろしいですか？");
	if ( myRet == true ){
		alert("停止しました。");
		return true;
    } else {
    	alert("キャンセルされました。");
    	return false;
    }
}
function continueConfirm(){
	myRet = window.confirm("このユーザーのアカウントを復活します。よろしいですか？");
	if ( myRet == true ){
		alert("復活しました。");
		return true;
    } else {
    	alert("キャンセルされました。");
    	return false;
    }
}
</script>
</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" /></li>
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="header">
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="signup">ユーザー新規登録</a>
				</c:if>
			</div>
			<div class="usersInfo">
				<c:forEach items="${usersInfo}" var="user">

						<div class="userInfo">
							<span class="account user">
								<c:out value="${user.account}" />
							</span>
							<span class="name user">
								<c:out value="${user.name}" />
							</span>
							<span class="branchName user">
								<c:out value="${user.branchName}" />
							</span>
							<span class="departmentName user">
								<c:out value="${user.departmentName}" />
							</span>
							<span class="isStopped user">
								<c:out value="${user.isStopped}" />
							</span>

							<form action="setting" method="get">
								<input type="hidden" name="userId" value="${user.id}">
								<input type="submit" value="編集" class="button">
							</form>

							<c:if test="${user.id != loginUser.id }">
								<c:if test="${user.isStopped != 0}">
									<form action="stop" method="post" onSubmit="return continueConfirm()">
										<input type="hidden" name="userId" value="${user.id}">
										<input type="submit" value="復活" class="button">
										<input type="hidden" name="isStopped"  value = 0>
									</form>
								</c:if>
								<c:if test="${user.isStopped == 0}">
									<form action="stop" method="post" onSubmit="return stopConfirm()">
										<input type="hidden" name="userId" value="${user.id}">
										<input type="hidden" name="isStopped" value = 1>
										<input type="submit" value="停止" class="button stop">
									</form>
								</c:if>
							</c:if>
						</div>
				</c:forEach>
			</div>
			<div class="copyright">Copyright(c)Takanori Sadatou</div>
		</div>
	</body>
</html>