<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header">
		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="management">ユーザー編集画面</a>
		</c:if>
	</div>

    <div class="main-contents">
      <c:if test="${ not empty errorMessages }">
        <div class="errorMessages">
          <ul>
            <c:forEach items="${errorMessages}" var="errorMessage">
              <li><c:out value="${errorMessage}" /></li>
            </c:forEach>
          </ul>
        </div>
      </c:if>
      <form action="setting" method="post"><br />
        <input name="id" value="${editUser.id}" id="id" type="hidden"/>
        <label for="account">アカウント名</label><br />

        <input name="account"  value="${editUser.account}" id="account" /><br />
        <label for="password">パスワード</label><br />

        <input name="password" type="password" id="password" /><br />
        <label for="checkPassword">確認用パスワード</label><br />

        <input name="checkPassword" type="password" id="checkPassword" /><br />
        <label for="name">名前</label><br/>

        <input name="name"  value="${editUser.name}" id="name" /><br />
        <label for="branchId">支社</label>

		<c:if test="${loginUser.id != editUser.id}">

        <select name="branchId">
          <c:forEach items="${branches}" var="branch">
            <c:if test="${editUser.branchId == branch.id}">
              <option value="${branch.id}" selected>
                <c:out value="${branch.name}" />
              </option>
            </c:if>

            <c:if test="${editUser.branchId != branch.id}">
              <option value="${branch.id}">
                <c:out value="${branch.name}" />
              </option>
            </c:if>

          </c:forEach>
        </select><br />
        <label for="departmentId">部署</label>
        <select name="departmentId">
          <c:forEach items="${departments}" var="department">
            <c:if test="${editUser.departmentId == department.id}">
              <option value="${department.id}" selected>
                <c:out value="${department.name}" />
              </option>
            </c:if>
            <c:if test="${editUser.departmentId != department.id}">
              <option value="${department.id}">
                <c:out value="${department.name}" />
              </option>
            </c:if>
          </c:forEach>
        </select><br/>

        </c:if>


        <c:if test="${loginUser.id == editUser.id}">

        <select name="branchId" disabled>
          <c:forEach items="${branches}" var="branch">
            <c:if test="${editUser.branchId == branch.id}">
              <option value="${branch.id}" selected>
                <c:out value="${branch.name}" />
              </option>
            </c:if>

            <c:if test="${editUser.branchId != branch.id}">
              <option value="${branch.id}">
                <c:out value="${branch.name}" />
              </option>
            </c:if>
          </c:forEach>
        </select><br />
        <label for="departmentId">部署</label>
        <select name="departmentId" disabled>
          <c:forEach items="${departments}" var="department">
            <c:if test="${editUser.departmentId == department.id}">
              <option value="${department.id}" selected>
                <c:out value="${department.name}" />
              </option>
            </c:if>
            <c:if test="${editUser.departmentId != department.id}">
              <option value="${department.id}">
                <c:out value="${department.name}" />
              </option>
            </c:if>
          </c:forEach>
        </select><br/>

        </c:if>

        <input type="submit" value="更新"  class="button"/><br />
      </form>
      <a href="./management">戻る</a>
      <div class="copyright">Copyright(c)Takanori Sadatou</div>
    </div>
</body>
</html>