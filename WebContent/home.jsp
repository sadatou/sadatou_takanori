<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function deleteConfirm(){
	myRet = window.confirm("本当に削除します。よろしいですか？");
	if ( myRet == true ){
		alert("削除しました。");
		return true;
    } else {
    	alert("キャンセルされました。");
    	return false;
    }
}
</script>
</head>
<body>
	<div class="header">
		<a href="message" class="link">新規投稿</a>
		<c:if test  = "${loginUser.branchId == 1 && loginUser.departmentId == 1 }">
			<a href="management" class="link">ユーザー管理</a>
		</c:if>
		<a href="logout" class="link">ログアウト</a>
	</div>

	    <div class="main-contents">
	    	<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="search">
				<form action="./"  method="get">
		        	<label for="date">日付</label>
		        	<input type="date" name="start" id="date"  value ="${strStart}"/>
		        	<input type="date" name="end" id="date" value ="${strEnd}"/><br />

					<label for="searchCategory">カテゴリ</label>
					<input name="searchCategory" id="searchCategory"  value="${preSearchCategory}" />
					<input type="submit" value="絞込み" class="button" />
			</form>
		</div>

        	<div class="messages">
				<c:forEach items="${messages}" var="message">
					<div class="message">
						<div class="account-name">

							<span class="userName">
								<c:out value="${message.userName}" />
							</span>

							<span class="category">
								【<c:out value="${message.category}" />】
							</span>

							 <span class="date">
							 	<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
							</span> <br />

							<span class="title">
								<c:out value="${message.title}" />
							</span>

						</div>

						<div class="text">
							<pre><samp><c:out value="${message.text}" /></samp></pre>
						</div>

						<c:if test="${ message.userId == loginUser.id}">
							<form action="deleteMessage" method="post" onSubmit="return deleteConfirm()">
								<input type="hidden" name="deletedId" value="${message.id}">
								<input type="submit" value="削除" class="deleteButton">
							</form>
						</c:if>

						<br />
						<c:forEach items="${comments}" var="comment">
							<c:if test="${ comment.messageId == message.id }">
								<div class="comment">
									<div class="comment-form">

										<span class="userName">
											<c:out value="${comment.userName}" />
										</span>

										<span class="date">
											<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
										</span>

										<div class="text">
											<pre><samp><c:out value="${comment.text}" /></samp></pre>
										</div>

										<c:if test="${ comment.userId == loginUser.id}">
											<form action="deleteComment" method="post" onSubmit="return deleteConfirm()">
												<input type="hidden" name="deletedId" value="${comment.id}">
												<input type="submit" value="コメント削除" class="deleteButton">
											</form>
										</c:if>

									</div>
								</div>
							</c:if>

						</c:forEach>
						<div class="comment-area">
							<form action="comment" method="post">
								<input type="hidden" name="messageId" value="${message.id}">
								<span>
									コメント入力欄 <br />
								</span>
								<textarea name="comment" cols="60" rows="5" class="comment-box"></textarea><br />
								<input type="submit" value="投稿" class="button">（500文字まで）
							</form>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	<div class="copyright">Copyright(c)Takanori Sadatou</div>
</body>
</html>