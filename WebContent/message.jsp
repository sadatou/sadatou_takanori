<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" /></li>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<form action="message" method="post"><br />

			<label for="title">件名 </label>
			<input name="title" id="title" value="${message.title}" /><br />

			<label for="category">カテゴリ</label>
			<input name="category" id="category" value="${message.category}" /><br />

			<label for="message-box">投稿内容</label>
			<textarea name="text" cols="100" rows="5" id="message-box"><c:out value="${message.text}" /></textarea>

			<input type="submit" value="投稿" class="button" /> <br />
		</form>
		<a href="./">戻る</a>
		<div class="copyright">Copyright(c)Takanori Sadatou</div>
	</div>
</body>
</html>