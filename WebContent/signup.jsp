<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<a href="./">ホーム</a>
		<a href="management">ユーザー管理</a>
	</div>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="signup" method="post"><br />

			<label for="account">アカウント名</label>
			<input name="account" id="account" value="${user.account }" />

			<label for="password">パスワード</label>
			<input name="password" type="password" />

			<label for="checkPassword">確認用パスワード</label>
			<input name="checkPassword" type="password" />

			<label for="name">名前</label>
			<input name="name" id="name" value="${user.name }" />

			<label for="branchId">支社</label>
			<select name="branchId" >
				<c:forEach items="${branches}" var="branch">
					<c:if test="${user.branchId == branch.id}">
						<option value="${branch.id}" selected>
							<c:out value="${branch.name}" />
						</option>
					</c:if>
					<c:if test="${user.branchId != branch.id}">
						<option value="${branch.id}">
							<c:out value="${branch.name}" />
						</option>
					</c:if>
				</c:forEach>
			</select><br />

			<label for="departmentId">部署</label>
			<select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<c:if test="${user.departmentId == department.id}">
						<option value="${department.id}" selected>
							<c:out value="${department.name}" />
						</option>
					</c:if>
					<c:if test="${user.departmentId != department.id}">
						<option value="${department.id}">
							<c:out value="${department.name}" />
						</option>
					</c:if>
				</c:forEach>
			</select><br />

			<input type="submit" value="登録" class="button" /><br />
			<a href="management">戻る </a>

		</form>
		<div class="copyright">Copyright(c)Takanori Sadatou</div>
	</div>
</body>
</html>
