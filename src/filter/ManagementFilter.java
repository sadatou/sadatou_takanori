package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/setting", "/management", "/signup" })
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		User user = (User) session.getAttribute("loginUser");
		String errorMessage = "管理者のみ閲覧できるページです。";

		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (branchId != 1 || departmentId != 1) {	// 本社と総務人事部以外
			session.setAttribute("errorMessages", errorMessage);
			((HttpServletResponse)response).sendRedirect("./");
			} else {
				chain.doFilter(request, response);
			}
		}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}