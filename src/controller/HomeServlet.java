package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	String strStart = request.getParameter("start");
		String strEnd = request.getParameter("end");
		String searchCategory = request.getParameter("searchCategory");

    	List<UserMessage> messages = new MessageService().select(strStart, strEnd, searchCategory);
    	List<UserComment> comments = new CommentService().select();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("strStart", strStart);
		request.setAttribute("strEnd", strEnd);
		request.setAttribute("preSearchCategory", searchCategory);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
