package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static int editUserId;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		User loginUser = (User) request.getSession().getAttribute("loginUser");
		String strEditUserId = request.getParameter("userId");

		boolean isError = true;

		if (strEditUserId == null || strEditUserId == "") {
			isError = false;
		} else if (!strEditUserId.matches("^[0-9]+$")) {
			isError = false;
		}

		if (isError == false) {
			HttpSession session = request.getSession();
			String errorMessage = "不正なパラメータが入力されました";
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("management");
			return;
		}

		editUserId = Integer.parseInt(strEditUserId);
		User editUser = new UserService().select(editUserId);

		if (editUser == null) {
			HttpSession session = request.getSession();
			String errorMessage = "不正なパラメータが入力されました";
			session.setAttribute("errorMessages", errorMessage);
			response.sendRedirect("management");
			return;
		}

		if (loginUser.getId() == editUserId) {
			List<Branch> branch = new ArrayList<Branch>();
			List<Department> department = new ArrayList<Department>();
			branch.add(branches.get(0));
			department.add(departments.get(0));
			branches = branch;
			departments = department;
		}

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("setting.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		User editUser = new UserService().select(editUserId);

		if (isValid(errorMessages, user, editUser)) {
		try {

			new UserService().update(user);

		} catch (NoRowsUpdatedRuntimeException e) {
			errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		else {
			request.setAttribute("errorMessages", errorMessages);
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		response.sendRedirect("management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setcheckPassword(request.getParameter("checkPassword"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(List<String> errorMessages, User user, User editUser) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String checkPassword = user.getcheckPassword();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウントは6文字以上で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		} else if (!account.matches("^([a-zA-Z0-9]{6,20})$")) {
			errorMessages.add("アカウント名は半角英数字かつ6文字以上20字以下で入力してください");
		} else if ((!editUser.getAccount().equals(user.getAccount())) &&
				new UserService().checkDuplication(account)) {
			errorMessages.add("アカウントが重複しています");
		}

		if (!StringUtils.isEmpty(password)) {
			if (6 > password.length()) {
				errorMessages.add("パスワードは6文字以上で入力してください");
			} else if (20 < password.length()) {
				errorMessages.add("パスワードは20文字以下で入力してください");

			} else if (!password.matches
				("^[a-zA-Z0-9!\"#$%&'()=-~^|\\`@{[+;*:}]<,>.?/_]{6,20}+$")) {
				errorMessages.add("パスワードは半角英数字記号のみ、6文字以上20字以下で入力してください");
			}

			if (!password.equals(checkPassword)) {
				errorMessages.add("パスワードが一致しません");
			}
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if (StringUtils.isBlank(Integer.toString(branchId))) {
			errorMessages.add("支社を入力してください");
		}

		if (StringUtils.isBlank(Integer.toString(departmentId))) {
			errorMessages.add("部署名を入力してください");
		}

		if (branchId == 1) {	// 本社
			if (!(departmentId == 1 || departmentId == 2)) {	// 総務人事と情報管理以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		} else {	// 支社
			if (!(departmentId == 3 || departmentId == 4)) {	// 営業と技術部以外
				errorMessages.add("支社と部署の組合せが不適切です");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}


}