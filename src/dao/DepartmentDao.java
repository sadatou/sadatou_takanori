package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments ";
			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department> departments = toDepartment(rs);
			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartment(ResultSet rs) throws SQLException {

		List<Department> departments = new ArrayList<Department>();
		try {
			while (rs.next()) {
				Department branch = new Department();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getTimestamp("created_date"));
				branch.setCreatedDate(rs.getTimestamp("updated_date"));

				departments.add(branch);
			}
			return departments;
		} finally {
			close(rs);
		}
	}
}