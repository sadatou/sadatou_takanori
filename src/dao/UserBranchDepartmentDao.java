package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> selectAllUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id as user_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as user_name, ");
			sql.append("    users.branch_id as branch_id, ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    users.department_id as department_id, ");
			sql.append("    departments.name as department_name, ");
			sql.append("    users.created_date as created_date, ");
			sql.append("    users.updated_date as updated_date, ");
			sql.append("    users.is_stopped as is_stopped ");

			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> usersInfo = toUserBranchDepartment(rs);
			return usersInfo;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> usersInfo = new ArrayList<UserBranchDepartment>();

		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();

				user.setId(rs.getInt("user_id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("user_name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setBranchName(rs.getString("branch_name"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setDepartmentName(rs.getString("department_name"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));
				user.setIsStopped(rs.getInt("is_stopped"));

				usersInfo.add(user);
			}
			return usersInfo;
		} finally {
			close(rs);
		}
	}
}
