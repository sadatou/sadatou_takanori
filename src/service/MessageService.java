package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection  = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String strStart, String strEnd, String searchCategory) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;

		if(strStart == "" || strStart == null) {
			strStart = "2020-01-01 00:00:00";
		}else {
			strStart = strStart + " 00:00:00";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = null;
		try {
			start = dateFormat.parse(strStart);
		} catch (ParseException e) {
			System.out.println(e);
		}

		Date end = null;
		if(strEnd == "" || strEnd == null) {
			end =  new Date();
		} else {
			strEnd = strEnd + " 23:59:59";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				end = sdf.parse(strEnd);
			} catch (ParseException e) {
				System.out.println(e);
			}
		}

		if (searchCategory == null) {
			searchCategory = "";
		}

		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, start, end, searchCategory, LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int deletedId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, deletedId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
